import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todos: [],
    btns: [
      { id: 0, text: "All", selected: false },
      { id: 1, text: "Active", selected: true },
      { id: 2, text: "Completed", selected: false },
    ],
  },

  mutations: {
    initTodos(state, todos) {
      state.todos = todos;
    },
    changeTodoState(state, id) {
      let todo = state.todos.find((el) => el.id === id);
      todo.completed = !todo.completed;
      localStorage.setItem("todos", JSON.stringify(state.todos));
    },
    addTodo(state, todoTitle) {
      let lastId =
        state.todos.length === 0
          ? 0
          : state.todos[state.todos.length - 1].id + 1;
      state.todos.push({ id: lastId, title: todoTitle, completed: false });
      localStorage.setItem("todos", JSON.stringify(state.todos));
    },
    deleteTodo(state, id) {
      let todo = state.todos.find((el) => el.id === id);
      let index = state.todos.indexOf(todo);
      state.todos.splice(index, 1);
      localStorage.setItem("todos", JSON.stringify(state.todos));
    },
    changeBtn(state, id) {
      state.btns.forEach((el) => (el.selected = false));
      let activeBtn = state.btns.find((el) => el.id === id);
      activeBtn.selected = true;
    },
  },

  actions: {
    init(context) {
      context.commit(
        "initTodos",
        JSON.parse(localStorage.getItem("todos") || "[]")
      );
    },
    changeTodoState(context, id) {
      context.commit("changeTodoState", id);
    },
    addTodo(context, todoTitle) {
      context.commit("addTodo", todoTitle);
    },
    deleteTodo(context, id) {
      context.commit("deleteTodo", id);
    },
    changeBtn(context, id) {
      context.commit("changeBtn", id);
    },
  },

  getters: {
    allTodos: (state) => {
      return state.todos;
    },
    completedTodos: (state, getters) => {
      return getters.allTodos
        .filter((el) => {
          return el.title && el.completed;
        })
        .reverse();
    },
    activeTodos: (state, getters) => {
      return getters.allTodos
        .filter((el) => {
          return el.title && !el.completed;
        })
        .reverse();
    },
    validTodos: (state, getters) => {
      return getters.allTodos
        .filter((el) => {
          return el.title;
        })
        .reverse();
    },
    btns: (state) => {
      return state.btns;
    },
    activeBtnTitle: (state) => {
      return state.btns.find((el) => el.selected).text;
    },
  },

  modules: {},
});
