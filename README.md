# taskList

The todo list is a simple but effective way for me to practice **Vue/Vuex**.

Live demo you can watch [here](https://arkhipova.gitlab.io/tasklist/). <br />

![Alt Image of the todo app](src/assets/visual.png)

## Build Setup

```bash
# clone repo
git clone https://gitlab.com/Arkhipova/tasklist.git

# install dependencies
npm i

# serve with hot reload at http://localhost:8080/
npm run serve

# build for production with minification
npm run build
```

## Explanation of characters in commits

`+` — added

`-` — deleted

`$` — updated

<br />

_The design of the project was made in [Figma](https://www.figma.com/file/94jaCLWsI75MJ5Tg93bByF/TODO-list?node-id=0%3A1)._
